var testimonialThumbs = new Swiper(".testimonial-thumbs", {
    spaceBetween: 10,
    slidesPerView: 3,
    centeredSlides: true,
    freeMode: true,
    autoplay: true,
    loop: true,
    speed: 3000
  });
  var testimonialContent = new Swiper(".testimonial-comment", {
    spaceBetween: 10,
    autoplay: true,
    loop: true,
    speed: 3000,
    thumbs: {
      swiper: testimonialThumbs
    },
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
      
    }
  });
 const backToTop = document.querySelector(".backToTop");
 window.addEventListener("scroll",()=>{
   if(window.pageYOffset >100){
     backToTop.classList.add("active");
   }else{
    backToTop.classList.remove("active");
   }
 })
